/*Домашнее задание
Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". 
Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты, 
"Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.
 */

let doc = {
	header : '',
	body : '',
	footer : '',
	data : '',

	application: {
		header : '' ,
		body : '',
		footer : '',
		data : ''
	},
	fillingDoc : function(){
		this.header = prompt ('Введите заголовок основного документа');
		this.body = prompt ('Введите текст основного документа');
		this.footer = prompt ('Введите футер основного документа');
		this.data = prompt ('Введите дату основного документа');

		this.application.header = prompt ('Введите заголовок вложенного документа');
		this.application.body = prompt ('Введите текст вложенного документа');
		this.application.footer = prompt ('Введите футер вложенного документа');
		this.application.data = prompt ('Введите дату вложенного документа');
	},

	display : function(){
		document.write ('<h3>' + this.header + '<br/>');
		document.write (this.body + '<br/>');
		document.write (this.footer + '<br/>');
		document.write (this.data + '<br/>');
	
		document.write ('<h4>' + this.application.header + '<br/>');
		document.write (this.application.body + '<br/>');
		document.write (this.application.footer + '<br/>');
		document.write (this.application.data + '<br/>');
	}

};

doc.fillingDoc();
doc.display();
